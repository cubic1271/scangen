/*
The MIT License (MIT)

Copyright (c) 2014 Gilbert Clark

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include "packet.h"

// Note: this was derived from boilerplate that has been floating around for 
// a long time.  Not quite sure of the original source, but it's quite old 
// and I don't believe there to be any restrictions on redistribution that
// would interfere with redistribution.
uint16_t construct_checksum(uint8_t *data, int len) {
	int32_t sum = 0;

	while(len > 1) {
		sum += *((uint16_t *) data);
		data += 2;
		if(sum & 0x80000000) {
			sum = (sum & 0xFFFF) + (sum >> 16);
		}
		len -= 2;
	}

	if(len) {
		sum += (uint16_t) *(uint8_t *)data;
	}

	while(sum >> 16) {
		sum = (sum & 0xFFFF) + (sum >> 16);
	}

	return ~sum;
}

void construct_pcap_file_hdr(pcap_hdr *hdr)
{
	hdr->magic_number = 0xa1b2c3d4;
	hdr->version_major = 2;
	hdr->version_minor = 4;
	hdr->thiszone = 0;
	hdr->sigfigs = 0;
	hdr->snaplen = 32768;
	hdr->network = 1;  // Ethernet
}

void construct_pcap_packet_hdr(pcaprec_hdr *hdr, uint32_t ts_sec, uint32_t ts_nsec, uint32_t pktlen)
{
	hdr->ts_sec = ts_sec;
	hdr->ts_usec = ts_nsec;
	hdr->incl_len = pktlen;
	hdr->orig_len = pktlen;
}

void construct_eth(uint8_t *packet)
{
	eth_hdr *eth = (eth_hdr *)packet;
	memset(eth, 0, sizeof(eth_hdr));
	for(int i = 0; i < 6; ++i) {
		eth->src[i] = rand() % 256;
		eth->dst[i] = rand() % 256;
	}
	eth->datalink = htons(DATALINK_IP4);
}

void construct_ip4(uint8_t *packet, uint8_t proto, uint32_t src_ip, uint32_t dst_ip)
{
	ip_hdr *ip = (ip_hdr *)(packet + sizeof(eth_hdr));
	memset(ip, 0, sizeof(ip_hdr));
	ip->ip_vhl = 0x45;
	ip->ip_len = htons(40);
	ip->ip_ttl = rand() % 50 + 189;
	ip->ip_p = proto;
	ip->ip_src = src_ip ? src_ip : ((rand() % 65536) << 16) | (rand() % 65536);
	ip->ip_dst = dst_ip ? dst_ip : ((rand() % 65536) << 16) | (rand() % 65536);
	ip->ip_sum = construct_checksum((uint8_t *)ip, 20);
}

void construct_tcp(uint8_t *packet, uint16_t srcport, uint16_t dstport, uint8_t flags, uint32_t seq, uint32_t ack)
{
	uint8_t ip_hdr_buf[20];
	ip_hdr *ip = (ip_hdr *)(packet + sizeof(eth_hdr));
	tcp_hdr *tcp = (tcp_hdr *)(packet + sizeof(eth_hdr) + sizeof(ip_hdr));
	memcpy(ip_hdr_buf, ip, 20);  // copy original IP header to buffer

	tcp_phdr *p_hdr = (tcp_phdr *)(((uint8_t *)tcp) - 12);  // re-use IP header space to build pseudo-header
	p_hdr->ip_src = ip->ip_src;
	p_hdr->ip_dst = ip->ip_dst;
	p_hdr->zero = 0;
	p_hdr->proto = PROTO_TCP;
	p_hdr->tcp_len = htons(20);

	memset(tcp, 0, sizeof(tcp_hdr));
	tcp->th_sport = srcport ? htons(srcport) : htons(rand() % 10000 + 32768);
	tcp->th_dport = dstport ? htons(dstport) : htons(rand() % 500);
	tcp->th_seq = htonl(seq);
	tcp->th_ack = htonl(ack);
	tcp->th_offx2 = 0x50;  // first nibble: TCP header length
	tcp->th_flags = flags;
	tcp->th_win = htons(32768);
	tcp->th_urp = 0;
	tcp->th_sum = construct_checksum((uint8_t *)p_hdr, 32);

	memcpy(ip, ip_hdr_buf, 20); // copy original IP header back into packet to overwrite pseudo-header
}
