/*
The MIT License (MIT)

Copyright (c) 2014 Gilbert Clark

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#ifndef _PACKET_H
#define _PACKET_H

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <netinet/in.h>

#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>

#include "pcap_struct.h"
#include "inet_struct.h"

#define TCP_SYN_FLAG (0x02)
#define TCP_RST_FLAG (0x04)
#define TCP_ACK_FLAG (0x10)

#define PROTO_TCP    (0x06)

uint16_t construct_checksum(uint8_t *data, int len);
void construct_pcap_file_hdr(pcap_hdr *hdr);
void construct_pcap_packet_hdr(pcaprec_hdr *hdr, uint32_t ts_sec, uint32_t ts_nsec, uint32_t pktlen);
void construct_eth(uint8_t *packet);
void construct_ip4(uint8_t *packet, uint8_t proto, uint32_t src_ip, uint32_t dst_ip);
void construct_tcp(uint8_t *packet, uint16_t srcport, uint16_t dstport, uint8_t flags, uint32_t seq, uint32_t ack);

#endif

