/*
The MIT License (MIT)

Copyright (c) 2014 Gilbert Clark

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <netinet/in.h>

#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <fcntl.h>

#include <assert.h>
#include <queue>

#include "packet.h"

// If there's no O_LARGEFILE on the platform, just define as (0)
#ifndef O_LARGEFILE
	#define O_LARGEFILE (0)
#endif

#define PACKET_BUFFER_SIZE  (128)
#define WRITE_BUFFER_SIZE   (8 * 1024 * 1024)  // 8 MB

static uint8_t *write_buffer = NULL;

uint64_t pktbuf_flush(const int fd, const uint64_t watermark, const uint8_t *write_buffer, const uint64_t write_offset) {
    if(write_offset == 0) {
        return write_offset;
    }
    int res = 0;
    if(write_offset >= watermark) { 
		// fprintf(stderr, "Flushing %u bytes.n", write_offset); 
		fprintf(stderr, "."); 
		res = write(fd, write_buffer, write_offset); 
		if(res < 0) { 
			perror("flush: write failed"); 
			_exit(EXIT_FAILURE); 
		}
		return 0; 
	}
    return write_offset;
}

void usage(const char *name)
{
	printf("%s [-p <parallel_scans>] [-c <scan_count>] [-h] [-r <rate>] [-t <duration>] <output>\n", name);
	_exit(EXIT_SUCCESS);
}

// Connection generator
int main(int argc, char *argv[])
{
    std::queue<uint8_t *> rst_pending;

	write_buffer = (uint8_t *)malloc(WRITE_BUFFER_SIZE);
	uint32_t write_offset = 0;
	char c = 0;
	int count = 100;
	int seed = 0;
    int parallel = 1;
	int duration = 1800;
	double rate = 1;
	char *path = NULL;

	while ((c = getopt (argc, argv, "c:hp:r:s:t:")) != -1) {
		switch (c)
		{
		case 'c':
			count = atoi(optarg);
			break;
        case 'p':
            parallel = atoi(optarg);
            break;
		case 'r':
			rate = atof(optarg);
			break;
		case 's':
			seed = atoi(optarg);
			break;
		case 't':
			duration = atoi(optarg);
			break;
		case '?':
			fprintf(stderr, "Invalid option usage: %c", optopt);
		default:
		case 'h':
			usage(argv[0]);
		}
	}

	if(!seed) {
		srand(time(0));
	}
	else {
		srand(seed);
	}

	if(optind == argc) {
		fprintf(stderr, "No trace output specified.\n");
		usage(argv[0]);
	}
	else {
		path = argv[optind];
	}

    if( !strcmp(path, "-") ) {
        path = "/dev/stdout";
    }
    else {
	    unlink(path);
    }
    
	int fd = open(path, O_CREAT | O_WRONLY | O_LARGEFILE, S_IRUSR | S_IWUSR);
	if(fd < 0) {
		perror("unable to open trace output");
		_exit(EXIT_FAILURE);
	}

	pcap_hdr file_header;
	construct_pcap_file_hdr(&file_header);
	int res = write(fd, &file_header, sizeof(file_header));
	if(res < 0) {
		perror("file header: write failed");
		_exit(EXIT_FAILURE);
	}
	uint64_t start_time = time(0);
	start_time *= 1000000000;
	uint64_t curr_time = start_time;

	assert(rate != 0);
	assert(count != 0);

	uint64_t packet_count = 1 + ((uint64_t)(( (rate * 1000000.0) / 8.0) * duration) / 54.0);
	uint64_t pkt_time_offset = (uint64_t)(1000000000 * (1.0 / (((rate * 1000000) / 8.0) / 54.0)));

    double first_rst_delay = (pkt_time_offset * count * parallel) / 1000000000.0;
    fprintf(stderr, "Computed max RTT (max time between SYN and ACK+RST): %0.2f seconds\n", first_rst_delay);
    if(first_rst_delay > 100) {
        fprintf(stderr, "NOTE: This is a high RTT and could fool analysis tools into thinking no response was sent to the SYN.\n");
    }

	fprintf(stderr, "Generating %lu packets  (~%lu MB of traffic).\n", packet_count, (packet_count * 54) / (1024 * 1024));

    uint32_t total_iterations = packet_count / count;

	uint32_t pkt_ctr = 0;
    uint8_t pkt_tmp[PACKET_BUFFER_SIZE];
	// we send packets in batches of two: one SYN, one RST
	for(uint64_t i = 0; i < total_iterations; i += 2) {
		uint32_t host   = ((rand() % 65536) << 16) | (rand() % 65536);
		uint32_t target = ((rand() % 65536) << 16) | (rand() % 65536);
		for(uint64_t host_ctr = 0; host_ctr < count; ++host_ctr) {
            uint8_t *packet = (uint8_t *)malloc(PACKET_BUFFER_SIZE);
			pcaprec_hdr pkt_hdr;			
			uint16_t src_port = rand() % 32768 + 10000;
			uint16_t dst_port = rand() % 1024;

			uint32_t seq = (rand() % 65536 << 16) | (rand() % 65000);
			uint32_t ack = 0;			

			// original SYN
			construct_pcap_packet_hdr(&pkt_hdr, curr_time / 1000000000, (curr_time % 1000000000) / 1000, 54);
			construct_eth(packet);
			construct_ip4(packet, PROTO_TCP, host, target);
			construct_tcp(packet, src_port, dst_port, TCP_SYN_FLAG, seq, ack);
			memcpy(write_buffer + write_offset, &pkt_hdr, sizeof(pkt_hdr));
			write_offset += sizeof(pkt_hdr);
			memcpy(write_buffer + write_offset, packet, 54);
			write_offset += 54;
			curr_time += pkt_time_offset;

			// RST
			ack = seq + 1;
			seq = 0;
			construct_pcap_packet_hdr(&pkt_hdr, curr_time / 1000000000, (curr_time % 1000000000) / 1000, 54);
			construct_eth(packet);
			construct_ip4(packet, PROTO_TCP, target, host);
			construct_tcp(packet, dst_port, src_port, TCP_RST_FLAG | TCP_ACK_FLAG, seq, ack);
            memcpy(pkt_tmp, &pkt_hdr, sizeof(pkt_hdr));
			//memcpy(write_buffer + write_offset, &pkt_hdr, sizeof(pkt_hdr));
			//write_offset += sizeof(pkt_hdr);
            memcpy(pkt_tmp + sizeof(pkt_hdr), packet, 54);
			//memcpy(write_buffer + write_offset, packet, 54);
			//write_offset += 54;
            memcpy(packet, pkt_tmp, 54 + sizeof(pkt_hdr));
            rst_pending.push(packet);

            // we have enough RSTs pending that we'll have 'parallel' hosts all with 'count' connections currently active and waiting for a RST...
            if(rst_pending.size() >= count * parallel) {
                uint8_t *rst_pkt = rst_pending.front();
                pcaprec_hdr *pkt_hdr = (pcaprec_hdr *)rst_pkt;
                pkt_hdr->ts_sec = curr_time / 1000000000;
                pkt_hdr->ts_usec = (curr_time % 1000000000) / 1000;
                memcpy(write_buffer + write_offset, rst_pkt, 54 + sizeof(pcaprec_hdr));
                write_offset += 54 + sizeof(pcaprec_hdr);
                free(rst_pkt);
                rst_pending.pop();
			    // only increment time offset when we actually write a packet.
                curr_time += pkt_time_offset;
            }

            write_offset = pktbuf_flush(fd, WRITE_BUFFER_SIZE - PACKET_BUFFER_SIZE - 1, write_buffer, write_offset);
		}
	}

    // write pending RST packets ...
    while(rst_pending.size() > 0) {
        uint8_t *rst_pkt = rst_pending.front();
        pcaprec_hdr *pkt_hdr = (pcaprec_hdr *)rst_pkt;
        pkt_hdr->ts_sec = curr_time / 1000000000;
        pkt_hdr->ts_usec = (curr_time % 1000000000) / 1000;
        memcpy(write_buffer + write_offset, rst_pkt, 54 + sizeof(pcaprec_hdr));
        write_offset += 54 + sizeof(pcaprec_hdr);
        free(rst_pkt);
        rst_pending.pop();
        curr_time += pkt_time_offset;

        write_offset = pktbuf_flush(fd, WRITE_BUFFER_SIZE - PACKET_BUFFER_SIZE - 1, write_buffer, write_offset);
    }

	// one last write to catch anything left in the buffer ...
    write_offset = pktbuf_flush(fd, 0, write_buffer, write_offset);
	
	fprintf(stderr, "\nDone.\n");
	close(fd);
}

