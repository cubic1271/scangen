/*
The MIT License (MIT)

Copyright (c) 2014 Gilbert Clark

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#ifndef INET_STRUCT_H
#define INET_STRUCT_H

#include <stdint.h>

	#define DATALINK_IP4  0x0800

	typedef uint8_t      hw_addr_t[6];
	typedef uint32_t     ipv4_addr_t;

	struct eth_hdr {
		hw_addr_t dst; 
		hw_addr_t src; 
		uint16_t datalink;
	} __attribute__((packed));

	struct ip_hdr {
		uint8_t ip_vhl;
		uint8_t ip_tos;
		uint16_t ip_len;
		uint16_t ip_id;
		uint16_t ip_off;
	#define IP_RF 0x8000
	#define IP_DF 0x4000
	#define IP_MF 0x2000
	#define IP_OFFMASK 0x1fff
		uint8_t ip_ttl;
		uint8_t ip_p;	
		uint16_t ip_sum;		
		ipv4_addr_t ip_src;
		ipv4_addr_t ip_dst; 
	} __attribute__((packed));

	#define IP_HL(ip)		(((ip)->ip_vhl) & 0x0f)
	#define IP_V(ip)		(((ip)->ip_vhl) >> 4)

	struct tcp_phdr {
		uint32_t ip_src;
		uint32_t ip_dst;
		uint8_t  zero;
		uint8_t  proto;
		uint16_t tcp_len;
	};

	struct tcp_hdr {
		uint16_t th_sport;
		uint16_t th_dport;
		uint32_t th_seq;
		uint32_t th_ack;

		uint8_t th_offx2;
	#define TH_OFF(th)	(((th)->th_offx2 & 0xf0) >> 4)
		uint8_t th_flags;
	#define TH_FIN 0x01
	#define TH_SYN 0x02
	#define TH_RST 0x04
	#define TH_PUSH 0x08
	#define TH_ACK 0x10
	#define TH_URG 0x20
	#define TH_ECE 0x40
	#define TH_CWR 0x80
	#define TH_FLAGS (TH_FIN|TH_SYN|TH_RST|TH_ACK|TH_URG|TH_ECE|TH_CWR)
		uint16_t th_win;
		uint16_t th_sum;
		uint16_t th_urp;
} __attribute__((packed));

#endif
